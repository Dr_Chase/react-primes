import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './App';
import Primes from '../../pages/primes/page.js';
import HomePage from '../../pages/primes/subComponents/header/page.js';


export default (
  <Route path="/" component={App}>
    <IndexRoute component={Primes} />
    <Route path="home" component={Primes} />
  </Route>
);
