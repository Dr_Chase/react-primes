import React from "react";
import styles from "./style.css";


export default class Header extends React.Component {
  render() {

     const style =  {
        textAlign: "center"
    };
    return (
      <div style={style}>
        <h1>Calculate the sum of the first N prime numbers.</h1>
      </div>
    );
  }
}
