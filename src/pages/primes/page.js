import React from 'react';
import { browserHistory } from 'react-router';

import { FormGroup, Form, ControlLabel, FormControl, Button, Label, Col } from 'react-bootstrap';
import Header from './subComponents/header/page.js';


export default class Primes extends React.Component {

    constructor() {
        super();

        this.state = {
            MAX_PRIMES: 5000,
            aPrimes: [],
            numOfPrimes: 0,
            sumOfPrimes: 0
        }
    }

    isItPrime = (nInput) => {
        let boolOutput = true;
        if (nInput === 1)   {
            return false;
        }
        else if (nInput === 2)   {
            return true;
        }
        else if ( (nInput % 2) == 0)    {
            return false;
        }
        else {
            for (let nIndex = 3; nIndex < nInput; nIndex +=2)  {
                if(nInput % nIndex === 0)   {
                    boolOutput = false;
                    break;
                }
            }
        }
        return boolOutput;
    }

    findPrimes = () =>  {
        let numOfPrimes = 0;
        for(let index = 0; numOfPrimes < this.state.MAX_PRIMES; index++)    {
            if( this.isItPrime(index) )   {
                this.state.aPrimes.push(index);
                ++numOfPrimes;
            }
        }
    }

    addPrimes = () =>   {
        const nInput = this.state.numOfPrimes;
        let nOutput = 0;
        for( let nIndex = 0; nIndex < nInput; nIndex++)  {
            nOutput += this.state.aPrimes[nIndex];
        }
        return nOutput;
    }
    handleChange = (event) =>   {
        this.setState({numOfPrimes: event.target.value})
    }
    handleClick = () => {
        this.findPrimes();
        this.setState({sumOfPrimes:  this.addPrimes()});
    }

  render() {

      let topDivStyle =     {
          margin: 30
      };
      let buttonStyle =     {
          marginLeft: 10
      }

    return (
        <div>
            <Header/>
            <div style={topDivStyle}  >
                <div className="col-md-offset-4">
                    <Form inline>
                        <FormGroup controlId="enterNumber">
                              <ControlLabel>Enter Number:</ControlLabel>
                              {' '}
                              <FormControl type="number" placeholder="1<= N <= 5000" onChange={this.handleChange} />
                        </FormGroup>
                        <Button style={buttonStyle} type="button" onClick={this.handleClick} >
                            calculate
                        </Button>
                        <Col smOffset={2}>
                            <h2><Label bsStyle="success">{this.state.sumOfPrimes}</Label>&nbsp;</h2>
                        </Col>

                    </Form>
                </div>
            </div>
        </div>


    );
  }
}
